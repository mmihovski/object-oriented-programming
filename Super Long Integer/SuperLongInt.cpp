#include "Header.h"
#include <iostream>
#include <cstring>

using namespace std;

SuperLongInt::SuperLongInt()
{
	lenght = 0;
	OverflowFlag = 0;
	NegativeFlag = 0;

	for (int i = 0; i < 100; i++)
	{
		Number[i] = '\0';
	}
}

SuperLongInt::SuperLongInt(char *number)
{
	int i, j;

	lenght = strlen(number);

	if (Number[0] == '-')
	{
		NegativeFlag = 1;
	}
	else
	{
		NegativeFlag = 0;
	}

	OverflowFlag = 0;

	for (i = 0, j = lenght - 1; j >= NegativeFlag; i++, j--)
	{
		Number[i] = number[j] - 48;
	}
	Number[i] = '\0';
	lenght = i;

	for (; i < 100; i++)
	{
		Number[i] = '\0';
	}

}

SuperLongInt::SuperLongInt(long int number)
{
	int i;

	if (number < 0)
	{
		NegativeFlag = 1;
		number = -number;
	}
	else NegativeFlag = 0;

	OverflowFlag = 0;

	for (i = 0; number > 0; i++)
	{
		Number[i] = number % 10;
		number /= 10;
	}

	Number[i] = '\0';
	lenght = i;

	for (; i < 100; i++)
	{
		Number[i] = '\0';
	}

}

SuperLongInt::SuperLongInt(const SuperLongInt &n)
{
	lenght = n.lenght;
	OverflowFlag = n.OverflowFlag;
	NegativeFlag = n.NegativeFlag;

	strcpy_s(Number, n.Number);
}

SuperLongInt SuperLongInt::operator= (SuperLongInt &n)
{
	lenght = n.lenght;
	OverflowFlag = n.OverflowFlag;
	NegativeFlag = n.NegativeFlag;

	strcpy_s(Number, n.Number);

	return *this;
}

SuperLongInt SuperLongInt::operator+ (SuperLongInt &n)
{
	SuperLongInt temp;
	int i;

	if (lenght >= n.lenght)
	{
		temp.lenght = lenght;
	}
	else
	{
		temp.lenght = n.lenght;
	}

	for (i = 0; i < temp.lenght; i++)
	{
		temp.Number[i] = Number[i] + n.Number[i] + temp.OverflowFlag;

		if (temp.Number[i] >= 10)
		{
			temp.Number[i] -= 10;
			temp.OverflowFlag = 1;
		}
		else temp.OverflowFlag = 0;
	}

	if (temp.OverflowFlag == 1)
	{
		temp.Number[i++] = 1;
		temp.OverflowFlag = 0;
	}

	temp.lenght = i;
	temp.Number[i] = '\0';

	for (; i < 100; i++)
	{
		temp.Number[i] = '\0';
	}
	
	for (i = temp.lenght - 1; i >= 0; i--)
	{
		cout << (char)(temp.Number[i] + 48);
	}

	cout << endl;

	return temp;
}

void SuperLongInt::PrintNumber()
{
	int i;

	if (NegativeFlag == 1)
	{
		cout << "-";
	}

	for (i = lenght - 1; i >= 0; i--)
	{
		cout << (char)(Number[i] + 48);
	}

	cout << endl;
}