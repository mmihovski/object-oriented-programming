#pragma once

class SuperLongInt
{
	char Number[100];
	int lenght;
	char OverflowFlag;
	char NegativeFlag;

public:
	SuperLongInt();
	SuperLongInt(char *number);
	SuperLongInt(long int number);
	SuperLongInt(const SuperLongInt &n);

	SuperLongInt operator= (SuperLongInt &n);
	SuperLongInt operator+ (SuperLongInt &n);

	void PrintNumber();
};