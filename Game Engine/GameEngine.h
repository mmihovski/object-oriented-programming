#pragma once
#include "Monster.h"
#include "Knight.h"
#include <typeinfo>
#include <cstring>

#define weight 30
#define height 24

class GameEngine
{
	GameUnit *Units[100];
	unsigned int CountUnit;

	void clear();
	void PrintScreen();
	TPoint FindTarget(TPoint coord, unsigned int team);

public: 
	GameEngine();
    void Add(GameUnit *Unit);
	void GameCycle();
};