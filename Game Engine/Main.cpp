#include "GameEngine.h"

int main()
{
	GameEngine Game;

	Knight K1;
	Monster M1;
	Monster M2;
	Monster M3;
	Knight K2;

	M2.changePos(TPoint(20, 15));
	M3.changePos(TPoint(1, 1));
    K2.changePos(TPoint(22, 1));

	Game.Add(&K1);
	Game.Add(&K2);
	Game.Add(&M1);
	Game.Add(&M2);
	Game.Add(&M3);
	
	for (int i = 0; i < 200; i++)
	{
		Game.GameCycle();
		Sleep(100);
	}

	getchar();
	return 0;
}