#pragma once
#include "GameUnit.h"

class Monster : public GameUnit 
{  
	unsigned int Nails;
	unsigned int Teeth;
public:
	Monster();
	void Attack(GameUnit **Units, int N);
	void Defend(int ASTR);
	void Move();
};
