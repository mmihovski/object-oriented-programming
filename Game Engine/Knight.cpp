#include "Knight.h"

Knight::Knight() : GameUnit()
{
	Sword = 1; Armor = 2;
	Position.x = 1; Position.y = 20;
	MoveSpeed = 2; AttackSpeed = 2;
	AttackRange = 2.0; Team = 1;
	Condition = 200;
}

void Knight::Attack(GameUnit **Units, int N)
{
	unsigned int AttackStrenght;
	double DIST;
	AttackStrenght = (Condition / 20 + 1) * (Sword / 2 + 1);

	for (int i = 0; i < N; i++)
	{
		DIST = sqrt((Units[i]->getPos().x - Position.x)*(Units[i]->getPos().x - Position.x) +
			        (Units[i]->getPos().y - Position.y)*(Units[i]->getPos().y - Position.y));

		if (Units[i]->getTeam() != Team)
		{
			if (DIST <= AttackRange)
			{
				Units[i]->Defend(AttackStrenght);
			}
		}
	}
}

void Knight::Move()
{
	if (cycles >= MoveSpeed)
	{
		GameUnit::Move();
		cycles = 0;
	}
	else
	{
		cycles++;
	}
}

void Knight::Defend(int ASTR)
{
	Condition = Condition - (ASTR - Armor);
	setCursorPosition(40, 15);
	printf("%d ", Condition); // <= debug
}
