#pragma once

#include <cmath>
#include <Windows.h>
#include <cstdio>

struct TPoint
{
	int x;
	int y;
	TPoint(){ x = -1; y = -1; }
	TPoint(int X, int Y) { x = X; y = Y; }
};

void setCursorPosition(int x, int y);


