#include "Monster.h"
#include <Windows.h>
#include <stdio.h>

Monster::Monster() : GameUnit()
{
	Nails = 1; Teeth = 1;
	Position.x = 20; Position.y = 5;
	MoveSpeed = 1; AttackSpeed = 4;
	AttackRange = 1.0; Team = 2;
}

void Monster::Attack(GameUnit **Units, int N)
{
	unsigned int AttackStrenght;
	double DIST;
	AttackStrenght = (Condition / 20 + 1) * ((Nails + Teeth) +  1);

	for (int i = 0; i < N; i++)
	{
		DIST = sqrt((Units[i]->getPos().x - Position.x)*(Units[i]->getPos().x - Position.x) +
			(Units[i]->getPos().y - Position.y)*(Units[i]->getPos().y - Position.y));

		if (Units[i]->getTeam() != Team)
		{
			if (DIST <= AttackRange)
			{
				Units[i]->Defend(AttackStrenght);
			}
		}
	}
}

void Monster::Move()
{
	if (cycles >= MoveSpeed)
	{
		GameUnit::Move();
		cycles = 0;
	}
	else
	{
		cycles++;
	}
}

void Monster::Defend(int ASTR)
{
	Condition = Condition - ASTR ;
	setCursorPosition(40, 10);
	printf("%d ", Condition);
}

