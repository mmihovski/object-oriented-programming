#pragma once
#include "Utility.h"

class GameUnit 
{
protected:
	TPoint Position , Target;
	int Condition;
	unsigned int MoveSpeed;
	unsigned int AttackSpeed;
	unsigned int Team;
	unsigned int cycles;
	double AttackRange;

public:
	GameUnit()
	{ 
		Condition = 100; 
		MoveSpeed = 1; 
		AttackSpeed = 1; 
		cycles = 0; 
	}
	TPoint getPos() { return Position; }
	unsigned int getMS() { return MoveSpeed; }
	virtual void Attack(GameUnit **Units, int N) = 0;
	virtual void Move();
	virtual void Defend(int ASTR) = 0;
	void setTarget(TPoint tar) { Target = tar; }
	TPoint getTarget() { return Target; }
	unsigned int getTeam() { return Team; }
	void changePos(TPoint t);
	bool live();
};

