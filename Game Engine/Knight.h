#pragma once
#include "GameUnit.h"

class Knight : public GameUnit
{   
	unsigned int Sword;
	unsigned int Armor;
public:
	Knight();
	void Attack(GameUnit **Units, int N);
	void Defend(int ASTR);
	void Move();
};          