#include "GameUnit.h"

void GameUnit::Move()
{   
	if (Target.x == -1 || Target.y == -1)
	{
		return;
	}

	if (Target.x > Position.x)
	{
		Position.x++;
	}
	else if (Target.x < Position.x)
	{
		Position.x--;
	}

	if (Target.y > Position.y)
	{
		Position.y++;
	}
	else if (Target.y < Position.y)
	{
		Position.y--;
	}
}

void GameUnit::changePos(TPoint t)
{
	Position = t;
}

bool GameUnit::live()
{
	if (Condition > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}