#include "GameEngine.h"

void setCursorPosition(int x, int y)
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	fflush(stdin);
	COORD coord = { (SHORT)x, (SHORT)y };
	SetConsoleCursorPosition(hOut, coord);
}

GameEngine::GameEngine()
{
	int i, j;

	for (i = 0; i < 100; i++)
	{
		Units[i] = 0;
	}
	CountUnit = 0;

	for (i = 0; i < height; i++)
	{
		for (j = 0; j < weight; j++)
		{
			if (j == 0 || j == weight - 1 || i == 0 || i == height - 1)
			{
				printf("#");
			}
			else
			{
				printf(" ");
			}
		}
		printf("\n");
	}
}

void GameEngine::Add(GameUnit *Unit)
{
	if (CountUnit >= 100)
	{
		return;
	}

	Units[CountUnit++] = Unit;
}

void GameEngine::PrintScreen()
{
	for (int i = 1; i < 29; i++)
	{
		for (int j = 1; j < 23; j++)
		{
			setCursorPosition(i, j);
			printf(" ");
		}
	}

	for (unsigned int i = 0; i < CountUnit; i++)
	{
		setCursorPosition(Units[i]->getPos().x, Units[i]->getPos().y);

		if (!strcmp(typeid(*Units[i]).name(), "class Knight"))
		{
			printf("K");
		}
		else if (!strcmp(typeid(*Units[i]).name(), "class Monster"))
		{
			printf("M");
		}
	}
}

TPoint GameEngine::FindTarget(TPoint coord, unsigned int Allied_team)
{
	TPoint targ;
	double Min = 10000.2;
	double DIST;

	for (unsigned int i = 0; i < CountUnit; i++)
	{   
		if (Allied_team != Units[i]->getTeam())
		{   
			DIST = sqrt((Units[i]->getPos().x - coord.x)*(Units[i]->getPos().x - coord.x) +
				             (Units[i]->getPos().y - coord.y)*(Units[i]->getPos().y - coord.y));

			if (Min > DIST)
			{
				Min = DIST;
				targ = Units[i]->getPos();
			}
		}
	}

	return targ;
}

void GameEngine::clear()
{
	for (unsigned int i = 0; i < CountUnit; i++)
	{
		if (!Units[i]->live())
		{
			for (unsigned int j = i; j < CountUnit - 1; j++)
			{
				Units[j] = Units[j + 1];
			}
			CountUnit--;
		}
	}
}

void GameEngine::GameCycle()
{
	PrintScreen();
	for (unsigned int i = 0; i < CountUnit; i++)
	{  
	   Units[i]->setTarget(FindTarget(Units[i]->getPos(), Units[i]->getTeam()));
	   Units[i]->Attack(Units, CountUnit);
	   clear();
       Units[i]->Move();
	}
}