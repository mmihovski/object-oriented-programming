#pragma once 

struct TableDescriptor
{
	char id[30];
	char type[10];
	char descriptor[5];
	unsigned int address;
	TableDescriptor() 
	{ 
		id[0] = '\0'; 
		type[0] = '\0'; 
		descriptor[0] = '\0'; 
		address = 0; 
	}
};