#include <iostream>
#include <cstring>
#include <fstream>

#include "ClassDescriptor.h"
#include "TDihotTable.h"
#include "THashTable.h"

using namespace std;

int SizeOfType(char *type)
{
	if (!strcmp(type, "char"))
		return 1;

	else if (!strcmp(type, "int"))
		return 2;

	else if (!strcmp(type, "float"))
		return 4;

	else if (!strcmp(type, "double"))
		return 8;

	return 0;
}

int main()
{
	ClassDescriptor D1;
	TDihotTable D2;
	THashTable D3;
	
	// Tables with sequential access
	D1.Add("d2", "char");
	D1.Add("d3", "int");
	D1.Add("d4", "float");
	D1.Add("d1", "double");

	D1.Remove("d3");

	cout << "\nTables with sequential access\n";
	D1.Print();


	//Tables with binary search
	ifstream Rfile("Data.txt");

	char buffer[100], buffer1[100];

	if (!Rfile.is_open())
	{
		cout << "R file not open" << endl;
		getchar();
		return 1;
	}

	while (Rfile >> buffer >> buffer1)
	{
		D2.Add(buffer, buffer1);
	}

	D2.PrintInFile();
	Rfile.close();

	cout << "\nTables with binary search\n";
	D2.Print();


	//Hash tables
	D3.Add("d3", "char");
	D3.Add("d2", "int");
	D3.Add("d4", "float");
	D3.Add("d1", "double");

	cout << "\nHash tables\n";
	D3.Print();

	getchar();
	return 0;
}

