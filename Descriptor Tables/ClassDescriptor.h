#pragma once 

#include "Descriptor.h"
#include <cstring>

using namespace std;

int SizeOfType(char *type);

class ClassDescriptor
{
	TableDescriptor table[100];
	int count;

public:
	ClassDescriptor();
	
	int Find(char *id);
	void Add(char *id, char *type);
	void Remove(char *id);
	void Print();
};
