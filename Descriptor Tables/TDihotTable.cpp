#include <iostream>
#include <cstring>
#include <fstream>
#include "TDihotTable.h"

using namespace std;

TDihotTable::TDihotTable()
{
	count = 0;
	address = 0;
	strcpy_s(Table[0].descriptor, "V1");
	Table[0].address = 0000;
}

int TDihotTable::BinarySearch(char *id)
{
	int left = 0;
	int right = count - 1;
	int middle;

	while (left <= right)
	{
		middle = (left + right) / 2;

		if (strcmp(Table[middle].id, id) == 1)
		{
			right = middle - 1;
		}
		else if (strcmp(Table[middle].id, id) == -1)
		{
			left = middle + 1;
		}
		else
		{
			return middle;
		}
	}
	return -left;
}

void TDihotTable::Add(char *id, char *type)
{
	int pos;

	if (count >= 100)
	{
		cout << "Full table\n";
		return;
	}
	else if (strcmp(type, "int") && strcmp(type, "float") && strcmp(type, "char") && strcmp(type, "double"))
	{
		cout << "Wrong type\n";
		return;
	}
	else
	{
		pos = BinarySearch(id);

		if (pos > 0)
		{
			cout << "Already exist\n";
			return;
		}
		else
		{
			pos = -pos;

			for (int i = count; i > pos; i--)
			{
				Table[i] = Table[i - 1];
			}

			strcpy_s(Table[pos].id, id);
			strcpy_s(Table[pos].type, type);

			if (!count)
			{
				strcpy_s(current_type, "char");
			}

			if (count)
			{
				sprintf_s(Table[pos].descriptor, "V%d", count + 1);

				address += SizeOfType(current_type);
				Table[pos].address = address;
				strcpy_s(current_type, type);
			}
		}
		count++;
	}
}

void TDihotTable::Remove(char *id)
{
	int i, n;
	n = BinarySearch(id);

	if (n >= 0)
	{
		for (i = n; i < count - 1; i++)
		{
			Table[i] = Table[i + 1];
		}
		count--;
	}
	else cout << "Not found element to remove" << endl;
}

void TDihotTable::Print()
{
	int i;

	for (i = 0; i < count; i++)
	{
		cout << Table[i].id << ' ' << Table[i].type << ' ' << Table[i].descriptor << ' ' << Table[i].address << '\n';
	}
}

void TDihotTable::PrintInFile()
{
	int i;

	ofstream Wfile("Target.txt");

	if (!Wfile.is_open())
	{
		cout << "W file not open" << endl;
		getchar();
		return ;
	}

	for (i = 0; i < count; i++)
	{
		Wfile << Table[i].id << ' ' << Table[i].type << ' ' << Table[i].descriptor << ' ' << Table[i].address << '\n';
	}
	Wfile.close();
}