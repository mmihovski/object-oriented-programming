#include <iostream>
#include <cstring>
#include "THashTable.h"

using namespace std;

int THashTable::HashFunction(char *id)
{
	int i;
	int sum = 0;

	for (i = 0; id[i]; i++)
	{
		sum += id[i];
	}

	return sum & 127;
}

int THashTable::Search(char* id)
{
	int index, ind_ret;
	index = HashFunction(id);

	if (index)
	{
		ind_ret = index - 1;
	}
	else
	{
		ind_ret = 127;
	}

	for (;;)
	{
		if (Table[index].id[0] == '\0' || !strcmp(Table[index].id, id))
		{
			return index;
		}

		else if (index == ind_ret)
		{
			return -1;
		}
		index++;

		if (index >= 128)
		{
			index = 0;
		}
	}
}

void THashTable::Add(char *id, char *type)
{
	int pos;
	pos = Search(id);

	if (pos == -1)
	{
		cout << "Full table\n";
		return;
	}
	else if (strcmp(type, "int") && strcmp(type, "float") && strcmp(type, "char") && strcmp(type, "double"))
	{
		cout << "Wrong type\n";
		return;
	}
	else if (!strcmp(Table[pos].id, id))
	{
		cout << "Already exist\n";
		return;
	}
	else
	{
		strcpy_s(Table[pos].id, id);
		strcpy_s(Table[pos].type, type);
		sprintf_s(Table[pos].descriptor, "V%d", Descript + 1);
		Table[pos].address = current_address;

		current_address += SizeOfType(type);
	}
	Descript++;
}

void THashTable::Print()
{
	for (int i = 0; i < 128; i++)
	{
		if (Table[i].id[0] != '\0')
		{
			cout << i << "\t"
				<< Table[i].id << "\t"
				<< Table[i].type << "\t"
				<< Table[i].descriptor << "\t"
				<< Table[i].address << endl;
		}
	}
}

void THashTable::Remove(char *id)
{
	int i;
	i = Search(id);

	if (!strcmp(Table[i].id, id))
	{
		Table[i].id[0] = '\0';
		Table[i].type[0] = '\0';
		Table[i].descriptor[0] = '\0';
		Table[i].address = 0;
	}
}