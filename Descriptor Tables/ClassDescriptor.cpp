#include <iostream>
#include <cstring>
#include "ClassDescriptor.h"

using namespace std;

ClassDescriptor::ClassDescriptor()
{
	count = 0;
	strcpy_s(table[0].descriptor, "V1");
	table[0].address = 0000;
}

int ClassDescriptor::Find(char *id)
{
	int i;

	for (i = 0; i < count; i++)
	{
		if (!strcmp(table[i].id, id))
		{
			return i;
		}
	}

	return -1;
}


void ClassDescriptor::Add(char *id, char *type)
{
	if (count >= 100)
	{
		cout << "Full table\n";
		return;
	}
	else if (Find(id) >= 0)
	{
		cout << "Already used\n";
		return;
	}
	else if ((strcmp(type, "int") && strcmp(type, "float") && strcmp(type, "char") && strcmp(type, "double")))
	{
		cout << "Wrong type\n";
		return;
	}
	else
	{
		strcpy_s(table[count].id, id);
		strcpy_s(table[count].type, type);

		if (count)
		{
			sprintf_s(table[count].descriptor, "V%d", count + 1);
			table[count].address = table[count - 1].address + SizeOfType(table[count - 1].type);
		}
		count++;
	}
}

void ClassDescriptor::Remove(char *id)
{
	int n;

	n = Find(id);

	if (n >= 0)
	{
		for (; n < count - 1; n++)
		{
			table[n] = table[n + 1];
		}
		count--;
	}
}

void ClassDescriptor::Print()
{
	int i;

	for (i = 0; i < count; i++)
	{
		cout << table[i].id << ' ' << table[i].type << ' ' << table[i].descriptor << ' ' << table[i].address << '\n';
	}
}


