#pragma once 

#include "Descriptor.h"

int SizeOfType(char *type);

class THashTable
{
	TableDescriptor Table[128];
	unsigned int current_address;
	int Descript;

public:
	THashTable()
	{
		current_address = 0;
		Descript = 0;
	}

	int HashFunction(char *id);
	int Search(char *id);
	void Add(char *id, char *type);
	void Remove(char *id);
	void Print();
};