#pragma once 

#include "Descriptor.h"
#include <cstring>
using namespace std;

int SizeOfType(char *type);

class TDihotTable
{
	TableDescriptor Table[100];
	int count;
	unsigned int address;
	char current_type[7];

public:
	TDihotTable();

	int BinarySearch(char *id);
	void Add(char *id, char *type);
	void Remove(char *id);
	void Print();
	void PrintInFile();
};