#include "Fibonacci.h"

Fibonacci::Fibonacci()
{
	f1 = f2 = 1;
}

int Fibonacci::Get()
{
	int n;

	n = f1 + f2;
	f1 = f2;
	f2 = n;

	return n;
}

int Fibonacci::Set(int n)
{
	while (Get() <= n);
	return Get();
}
