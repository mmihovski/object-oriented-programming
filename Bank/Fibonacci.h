#pragma once

class Fibonacci
{
	int f1, f2;

public:
	Fibonacci();
	
	int Get();
	int Set(int n);
};