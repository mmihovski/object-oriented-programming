#pragma once

#include "Fibonacci.h"
#include "ClientBank.h"
#include <iostream>

using namespace std;

class Bank
{
	ClientBank *TableClient;
	int TableSize;
	int TableCurrent;
	Fibonacci FNumber;

public:

	Bank(int number);
	~Bank();

	void AddClient(char *fname, char* lname);
	void AddAccount(int i, enum AccountType type, char *iban, double sum);
	void RemoveClient(int n);
	ClientBank &Client(int i);
	void Print(int i);
};
