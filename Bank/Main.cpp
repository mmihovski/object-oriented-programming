#include "Fibonacci.h"
#include "ClientBank.h"
#include "Bank.h"
#include <iostream>
#include <fstream>

using namespace std;

#define N 3

int main()
{
	Bank array(N);

	array.AddClient("Ivan", "Petrov");
	array.AddClient("Stoyan", "Kotev");
	array.AddClient("Dragan", "Cankov");

	ifstream Rfile("Data.txt");

	if (!Rfile.is_open())
	{
		cout << "Rfile is not open";
		return 1;
	}

	char iban[23];
	double sum;
	int type;
	int i;

	for ( i = 0; i < N; i++)
	{
		Rfile >> type >> iban >> sum;
		array.Client(i).AddAccount((AccountType)type, iban, sum);

		Rfile >> type >> iban >> sum;
		array.Client(i).AddAccount((AccountType)type, iban, sum);
	}

	Rfile.close();

	array.AddAccount(1, RDA, "BG80BNBG96698678543029", 35675);
	
	for (int i = 0; i < N; i++)
	{
		if (array.Client(i).TotalSum()>0)
		{
			array.Print(i);
		}
	}

	system("pause");
	return 0;
}