#include "Bank.h"
#include <iostream>
#include <fstream>

using namespace std;

Bank::Bank(int number)
{
	TableSize = FNumber.Set(number);
	TableCurrent = 0;

	TableClient = new ClientBank[TableSize];

}

Bank::~Bank() 
{ 
	delete[] TableClient; 
}

void Bank::AddClient(char *fname, char* lname)
{
	ClientBank *Buffer;
	int Size;

	if (TableCurrent >= TableSize)
	{
		Size = FNumber.Set(TableSize);

		Buffer = TableClient;

		TableClient = new ClientBank[Size];

		memcpy(TableClient, Buffer, sizeof(ClientBank)*TableCurrent);

		TableSize = Size;

		delete[] Buffer;
	}

	TableClient[TableCurrent].AddClient(fname, lname);
	TableCurrent++;
}

void Bank::RemoveClient(int n)
{
	int i;

	for (i = n; i < TableSize - 1; i++)
	{
		memcpy(&TableClient[i], &TableClient[i + 1], sizeof (ClientBank));
	}

	TableSize--;
}

ClientBank &Bank::Client(int i)
{
	return TableClient[i];
}

void Bank::Print(int i)
{
	Client(i).Print();
}

void Bank::AddAccount(int i, enum AccountType type, char *iban, double sum)
{
	TableClient[i].AddAccount(type, iban, sum);
}



