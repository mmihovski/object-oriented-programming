#pragma once

#include <cstring>

using namespace std;

enum AccountType
{
	CA,  // 0  Current Account
	SA,  // 1  Saving Account
	FDA, // 2  Fixed Deposit Account
	RDA, // 3  Recurring Deposit Account
	BDA, // 4  Bulk Deposit Account
};

struct Account
{
	enum AccountType Type;
	char IBAN[23];
	double Sum;
};

class ClientBank
{
	char Fname[15];
	char Lname[25];
	int AccountCount;
	Account TableAccount[10];

public:
	ClientBank(char* fname, char* lname);
	ClientBank();


	void AddClient(char* fname, char* lname);
	void AddAccount(enum AccountType type, char *iban, double sum);
	void RemoveAccount(char *iban);
	void AddSum(char *iban, double sum);
	Account * FindIBAN(char *iban);
	Account * FindSum(double sum);
	double TotalSum();
	void ClientBank::Print();

	
	
};