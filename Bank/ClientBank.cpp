#include "ClientBank.h"
#include <iostream>

using namespace std;

ClientBank::ClientBank(char* fname, char* lname)
{
	AccountCount = 0;
	strcpy_s(Fname, fname);
	strcpy_s(Lname, lname);
}

ClientBank::ClientBank()
{
	AccountCount = 0;
}

void ClientBank::AddClient(char* fname, char* lname)
{
	strcpy_s(Fname, fname);
	strcpy_s(Lname, lname);
}

void ClientBank::AddAccount(enum AccountType type, char *iban, double sum)
{
	TableAccount[AccountCount].Type = type;
	strcpy_s(TableAccount[AccountCount].IBAN, iban);
	TableAccount[AccountCount].Sum = sum;

	AccountCount++;
}

void ClientBank::RemoveAccount(char *iban)
{
	int i, k;

	for (i = 0; i < AccountCount; i++)
	{
		if (!strcmp(TableAccount[i].IBAN, iban))
		{
			k = i;
			break;
		}
	}

	for (i = k; i < AccountCount - 1; i++)
	{
		memcpy(&TableAccount[i], &TableAccount[i + 1], sizeof (struct Account));
	}

	AccountCount--;
}

void ClientBank::AddSum(char *iban, double sum)
{
	Account *p = NULL;

	p = FindIBAN(iban);

	if (p)
		p->Sum += sum;

	else cout << "Account not found ";
}


Account* ClientBank::FindIBAN(char *iban)
{
	int i;
	Account *p = NULL;

	for (i = 0; i<AccountCount; i++)
		if (!strcmp(TableAccount[i].IBAN, iban))
		{
			p = &TableAccount[i];
			break;
		}

	return p;
}

Account* ClientBank::FindSum(double sum)
{
	int i;

	for (i = 0; i<AccountCount; i++)
		if (TableAccount[i].Sum = sum)
			return &TableAccount[i];


	return  NULL;
}

double ClientBank::TotalSum()
{
	int i;
	double sum = 0;

	for (i = 0; i<AccountCount; i++)
		sum += TableAccount[i].Sum;

	return sum;

}

void ClientBank::Print()
{
	int i;

	cout << '\n' << Fname << ' ' << Lname << '\n';

	for (i = 0; i < AccountCount; i++)
	{
		cout << TableAccount[i].Type << '\t' << TableAccount[i].IBAN << '\t' << TableAccount[i].Sum << endl;
	}
}


